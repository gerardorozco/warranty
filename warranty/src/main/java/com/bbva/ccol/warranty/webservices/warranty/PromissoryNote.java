
package com.bbva.ccol.warranty.webservices.warranty;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para promissoryNote complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="promissoryNote">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numberTitle" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="entity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codeDepartment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codeCity " type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateExpedition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateExpiration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indicatorInsurance" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "promissoryNote", propOrder = {
    "numberTitle",
    "entity",
    "codeDepartment",
    "codeCity0020",
    "dateExpedition",
    "dateExpiration",
    "indicatorInsurance"
})
public class PromissoryNote {

    protected BigInteger numberTitle;
    protected String entity;
    protected String codeDepartment;
    @XmlElement(name = "codeCity ")
    protected String codeCity0020;
    protected String dateExpedition;
    protected String dateExpiration;
    protected BigInteger indicatorInsurance;

    /**
     * Obtiene el valor de la propiedad numberTitle.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberTitle() {
        return numberTitle;
    }

    /**
     * Define el valor de la propiedad numberTitle.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberTitle(BigInteger value) {
        this.numberTitle = value;
    }

    /**
     * Obtiene el valor de la propiedad entity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Define el valor de la propiedad entity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    /**
     * Obtiene el valor de la propiedad codeDepartment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeDepartment() {
        return codeDepartment;
    }

    /**
     * Define el valor de la propiedad codeDepartment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeDepartment(String value) {
        this.codeDepartment = value;
    }

    /**
     * Obtiene el valor de la propiedad codeCity0020.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeCity_0020() {
        return codeCity0020;
    }

    /**
     * Define el valor de la propiedad codeCity0020.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeCity_0020(String value) {
        this.codeCity0020 = value;
    }

    /**
     * Obtiene el valor de la propiedad dateExpedition.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateExpedition() {
        return dateExpedition;
    }

    /**
     * Define el valor de la propiedad dateExpedition.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateExpedition(String value) {
        this.dateExpedition = value;
    }

    /**
     * Obtiene el valor de la propiedad dateExpiration.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateExpiration() {
        return dateExpiration;
    }

    /**
     * Define el valor de la propiedad dateExpiration.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateExpiration(String value) {
        this.dateExpiration = value;
    }

    /**
     * Obtiene el valor de la propiedad indicatorInsurance.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIndicatorInsurance() {
        return indicatorInsurance;
    }

    /**
     * Define el valor de la propiedad indicatorInsurance.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIndicatorInsurance(BigInteger value) {
        this.indicatorInsurance = value;
    }

}
