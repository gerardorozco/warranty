package com.bbva.ccol.warranty.dao.impl;

import com.bbva.ccol.warranty.business.dto.DTOIntWarranty;
import com.bbva.ccol.warranty.dao.IWarrantyDAO;
import com.bbva.ccol.warranty.dao.tx.TxWarranty;
import com.bbva.ccol.warranty.webservices.warranty.Warranty;
import com.bbva.ccol.warranty.webservices.warranty.WarrantyService;
import com.bbva.ccol.warranty.webservices.warranty.mapper.IMapper;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;


@Component(value = "warrantyDAO")
public class WarrantyDAO implements IWarrantyDAO {

    @Resource(name = "wsMapper")
    IMapper mapper;

    @Resource(name = "warrantyService")
    WarrantyService warrantyService;

    @Resource(name = "txWarranty")
    TxWarranty txWarranty;


    @Override
    public void createWarranty(DTOIntWarranty dtoIntWarranty) {

        Warranty warranty = mapper.mapWarranty(dtoIntWarranty);
        warrantyService.createWarranty(warranty);

        txWarranty.invoke(dtoIntWarranty);
    }
}

