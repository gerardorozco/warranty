package com.bbva.ccol.warranty.facade.v01.dto;

import java.math.BigDecimal;

/**
 * Created by Entelgy on 22/08/2016.
 */
public class Value {

    private BigDecimal amountnumber;

    private String currencystring;

    public BigDecimal getAmountnumber() {
        return amountnumber;
    }

    public void setAmountnumber(BigDecimal amountnumber) {
        this.amountnumber = amountnumber;
    }

    public String getCurrencystring() {
        return currencystring;
    }

    public void setCurrencystring(String currencystring) {
        this.currencystring = currencystring;
    }
}
