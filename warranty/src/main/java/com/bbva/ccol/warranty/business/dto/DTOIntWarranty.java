
package com.bbva.ccol.warranty.business.dto;


import java.util.Date;
import java.util.List;

public class DTOIntWarranty {

    public final static long serialVersionUID = 1L;

    private DTOIntValue value;

    private Date registryDatedate;

    private Date expeditionDatedate;

    private Date expirationDatedate;

    private DTOIntWarrantyType warrantyType;

    private DTOIntInsuranceType insuranceType;

    private DTOIntFeatures features;

    private DTOIntWarrantyPlace warrantyPlace;

    private DTOIntAppraisal appraisal;

    private List<DTOIntDocuments> documents;

    public DTOIntWarrantyType getWarrantyType() {
        return warrantyType;
    }

    public void setWarrantyType(DTOIntWarrantyType warrantyType) {
        this.warrantyType = warrantyType;
    }

    public DTOIntValue getValue() {
        return value;
    }

    public void setValue(DTOIntValue value) {
        this.value = value;
    }

    public DTOIntWarrantyPlace getWarrantyPlace() {
        return warrantyPlace;
    }

    public void setWarrantyPlace(DTOIntWarrantyPlace warrantyPlace) {
        this.warrantyPlace = warrantyPlace;
    }

    public DTOIntInsuranceType getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(DTOIntInsuranceType insuranceType) {
        this.insuranceType = insuranceType;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public DTOIntFeatures getFeatures() {
        return features;
    }

    public void setFeatures(DTOIntFeatures features) {
        this.features = features;
    }

    public DTOIntAppraisal getAppraisal() {
        return appraisal;
    }

    public void setAppraisal(DTOIntAppraisal appraisal) {
        this.appraisal = appraisal;
    }

    public List<DTOIntDocuments> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DTOIntDocuments> documents) {
        this.documents = documents;
    }

    public Date getExpirationDatedate() {
        return expirationDatedate;
    }

    public void setExpirationDatedate(Date expirationDatedate) {
        this.expirationDatedate = expirationDatedate;
    }

    public Date getExpeditionDatedate() {
        return expeditionDatedate;
    }

    public void setExpeditionDatedate(Date expeditionDatedate) {
        this.expeditionDatedate = expeditionDatedate;
    }

    public Date getRegistryDatedate() {
        return registryDatedate;
    }

    public void setRegistryDatedate(Date registryDatedate) {
        this.registryDatedate = registryDatedate;
    }

}
