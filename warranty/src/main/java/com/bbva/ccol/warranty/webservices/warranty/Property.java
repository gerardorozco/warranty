
package com.bbva.ccol.warranty.webservices.warranty;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para property complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="property">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="typeEstate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="useLife" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mortgage" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="DestructibleArea" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="totalArea" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="constructedArea" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="propertyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="addressDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deed" type="{http://warranty}deed" minOccurs="0"/>
 *         &lt;element name="tenementType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="warranty" type="{http://warranty}warranty" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "property", propOrder = {
    "typeEstate",
    "useLife",
    "mortgage",
    "destructibleArea",
    "totalArea",
    "constructedArea",
    "propertyType",
    "addressTypeCode",
    "addressDescription",
    "phone",
    "deed",
    "tenementType",
    "warranty"
})
public class Property {

    protected String typeEstate;
    protected String useLife;
    protected Long mortgage;
    @XmlElement(name = "DestructibleArea")
    protected BigDecimal destructibleArea;
    protected BigDecimal totalArea;
    protected BigDecimal constructedArea;
    protected String propertyType;
    @XmlElement(name = "AddressTypeCode")
    protected String addressTypeCode;
    protected String addressDescription;
    protected String phone;
    protected Deed deed;
    protected String tenementType;
    protected Warranty warranty;

    /**
     * Obtiene el valor de la propiedad typeEstate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeEstate() {
        return typeEstate;
    }

    /**
     * Define el valor de la propiedad typeEstate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeEstate(String value) {
        this.typeEstate = value;
    }

    /**
     * Obtiene el valor de la propiedad useLife.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseLife() {
        return useLife;
    }

    /**
     * Define el valor de la propiedad useLife.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseLife(String value) {
        this.useLife = value;
    }

    /**
     * Obtiene el valor de la propiedad mortgage.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMortgage() {
        return mortgage;
    }

    /**
     * Define el valor de la propiedad mortgage.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMortgage(Long value) {
        this.mortgage = value;
    }

    /**
     * Obtiene el valor de la propiedad destructibleArea.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDestructibleArea() {
        return destructibleArea;
    }

    /**
     * Define el valor de la propiedad destructibleArea.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDestructibleArea(BigDecimal value) {
        this.destructibleArea = value;
    }

    /**
     * Obtiene el valor de la propiedad totalArea.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalArea() {
        return totalArea;
    }

    /**
     * Define el valor de la propiedad totalArea.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalArea(BigDecimal value) {
        this.totalArea = value;
    }

    /**
     * Obtiene el valor de la propiedad constructedArea.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getConstructedArea() {
        return constructedArea;
    }

    /**
     * Define el valor de la propiedad constructedArea.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setConstructedArea(BigDecimal value) {
        this.constructedArea = value;
    }

    /**
     * Obtiene el valor de la propiedad propertyType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropertyType() {
        return propertyType;
    }

    /**
     * Define el valor de la propiedad propertyType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropertyType(String value) {
        this.propertyType = value;
    }

    /**
     * Obtiene el valor de la propiedad addressTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressTypeCode() {
        return addressTypeCode;
    }

    /**
     * Define el valor de la propiedad addressTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressTypeCode(String value) {
        this.addressTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad addressDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressDescription() {
        return addressDescription;
    }

    /**
     * Define el valor de la propiedad addressDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressDescription(String value) {
        this.addressDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad phone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Define el valor de la propiedad phone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Obtiene el valor de la propiedad deed.
     * 
     * @return
     *     possible object is
     *     {@link Deed }
     *     
     */
    public Deed getDeed() {
        return deed;
    }

    /**
     * Define el valor de la propiedad deed.
     * 
     * @param value
     *     allowed object is
     *     {@link Deed }
     *     
     */
    public void setDeed(Deed value) {
        this.deed = value;
    }

    /**
     * Obtiene el valor de la propiedad tenementType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTenementType() {
        return tenementType;
    }

    /**
     * Define el valor de la propiedad tenementType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTenementType(String value) {
        this.tenementType = value;
    }

    /**
     * Obtiene el valor de la propiedad warranty.
     * 
     * @return
     *     possible object is
     *     {@link Warranty }
     *     
     */
    public Warranty getWarranty() {
        return warranty;
    }

    /**
     * Define el valor de la propiedad warranty.
     * 
     * @param value
     *     allowed object is
     *     {@link Warranty }
     *     
     */
    public void setWarranty(Warranty value) {
        this.warranty = value;
    }

}
