
package com.bbva.ccol.warranty.facade.v01.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.wordnik.swagger.annotations.ApiModelProperty;



@XmlRootElement(name = "warranty", namespace = "urn:com:bbva:ccol:warranty:facade:v01:dto")
@XmlType(name = "warranty", namespace = "urn:com:bbva:ccol:warranty:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Warranty
    implements Serializable
{

    public final static long serialVersionUID = 1L;

    private Value value;

    private Date registryDatedate;

    private Date expeditionDatedate;

    private Date expirationDatedate;

    private WarrantyType warrantyType;

    private InsuranceType insuranceType;

    private Features features;

    private WarrantyPlace warrantyPlace;

    private Appraisal appraisal;

    private List<Documents> documents;

    public WarrantyType getWarrantyType() {
        return warrantyType;
    }

    public void setWarrantyType(WarrantyType warrantyType) {
        this.warrantyType = warrantyType;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public WarrantyPlace getWarrantyPlace() {
        return warrantyPlace;
    }

    public void setWarrantyPlace(WarrantyPlace warrantyPlace) {
        this.warrantyPlace = warrantyPlace;
    }

    public InsuranceType getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(InsuranceType insuranceType) {
        this.insuranceType = insuranceType;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Features getFeatures() {
        return features;
    }

    public void setFeatures(Features features) {
        this.features = features;
    }

    public Appraisal getAppraisal() {
        return appraisal;
    }

    public void setAppraisal(Appraisal appraisal) {
        this.appraisal = appraisal;
    }

    public List<Documents> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Documents> documents) {
        this.documents = documents;
    }

    public Date getExpirationDatedate() {
        return expirationDatedate;
    }

    public void setExpirationDatedate(Date expirationDatedate) {
        this.expirationDatedate = expirationDatedate;
    }

      public Date getExpeditionDatedate() {
        return expeditionDatedate;
    }

    public void setExpeditionDatedate(Date expeditionDatedate) {
        this.expeditionDatedate = expeditionDatedate;
    }

    public Date getRegistryDatedate() {
        return registryDatedate;
    }

    public void setRegistryDatedate(Date registryDatedate) {
        this.registryDatedate = registryDatedate;
    }

}
