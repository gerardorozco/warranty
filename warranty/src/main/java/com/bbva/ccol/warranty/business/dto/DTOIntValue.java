package com.bbva.ccol.warranty.business.dto;

import java.math.BigDecimal;

/**
 * Created by Entelgy on 22/08/2016.
 */
public class DTOIntValue {

    private BigDecimal amountnumber;

    private String currencystring;

    public BigDecimal getAmountnumber() {
        return amountnumber;
    }

    public void setAmountnumber(BigDecimal amountnumber) {
        this.amountnumber = amountnumber;
    }

    public String getCurrencystring() {
        return currencystring;
    }

    public void setCurrencystring(String currencystring) {
        this.currencystring = currencystring;
    }

}
