package com.bbva.ccol.warranty.dao.tx.mapper;

import com.bbva.ccol.warranty.business.dto.DTOIntWarranty;
import com.bbva.ccol.warranty.dao.model.uge5.FormatoUGE5E00;

/**
 * Created by Entelgy on 23/08/2016.
 */
public interface ITxWarrantyMapper {

    FormatoUGE5E00 mapDtoInToRequestFormat(DTOIntWarranty dtoIn);

}
