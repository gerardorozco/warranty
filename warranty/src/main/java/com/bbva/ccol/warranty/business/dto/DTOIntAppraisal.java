package com.bbva.ccol.warranty.business.dto;

/**
 * Created by Entelgy on 22/08/2016.
 */
public class DTOIntAppraisal {

    private String datestring;

    private String appraiserstring;

    public String getDatestring() {
        return datestring;
    }

    public void setDatestring(String datestring) {
        this.datestring = datestring;
    }

    public String getAppraiserstring() {
        return appraiserstring;
    }

    public void setAppraiserstring(String appraiserstring) {
        this.appraiserstring = appraiserstring;
    }
}
