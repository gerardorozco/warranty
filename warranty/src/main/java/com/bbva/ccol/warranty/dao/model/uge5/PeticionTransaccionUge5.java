package com.bbva.ccol.warranty.dao.model.uge5;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>UGE5</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionUge5</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionUge5</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: CCTUGE5.txt
 * UGE5ALTAS GARANTIAS EKIP               UG        UG1CUGE5BVDKNPO UGE5E00             UGE5  NS3000NNNNNN    SSTN    C   SNNSSNNN  NN                2016-08-12CICSDC112016-08-2016.30.12CE32719 2016-08-12-09.03.22.998873CICSDC110001-01-010001-01-01
 * FICHERO: FDFUGE5E00.txt
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|01|00016|TIGARID|TIPO DE GARANTIA    |A|003|0|R|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|02|00022|NOMGARA|NOMBRE GARANTIA     |A|020|0|R|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|03|00045|VALGARA|VALOR DE GARANTIA   |A|016|0|R|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|04|00064|FECREGA|FECHA REGISTRA GARAN|A|008|0|O|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|05|00075|FECEXPD|FECHA EXPED DEL DOCU|A|008|0|R|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|06|00086|FECVEND|FECHA VENCI DEL DOCU|A|008|0|O|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|07|00097|PAISGAR|COD PAIS GARANTIA   |A|003|0|R|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|08|00103|GAIDRES|ID GAR RELA CON SEGU|A|003|0|R|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|09|00109|GANORES|NOM GAR REL CON SEGU|A|020|0|R|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|10|00132|TIFUNGA|TIPO FUNCION GARANTI|A|003|0|R|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|11|00138|DEFUNGA|DESCRIP FUNCION GARA|A|030|0|R|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|12|00171|FECTIME|FECHA TIMESTAMP     |A|026|0|R|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|13|00200|NOMEVAG|NOMBRE DEL EVALUADOR|A|020|0|R|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|14|00223|IDUNASD|ID UNICO ASOCIA A DO|A|008|0|R|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|15|00234|TIPDOCU|TIPO DE DOCUMENTO   |A|001|0|O|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|16|00238|NUMDOCU|NUMERO DE DOCUMENTO |A|015|0|O|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|17|00256|PRODOCU|PROPIE  DE DOCUMENTO|A|001|0|O|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|18|00260|FECCRED|FECHA CREACION DOCUM|A|008|0|O|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|19|00271|FECVEDO|FECHA VENCIMI DOCUME|A|008|0|O|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|20|00282|TIVAASD|TIPO VALOR ASOCI DOC|A|003|0|O|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|21|00288|CAVAASD|VALOR ASOCIA AL DOCU|A|016|0|O|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|22|00307|MONASD |MONEDA ASOCIAD DOCUM|A|003|0|O|        |
 * UGE5E00 |S - ALTA DATOS GARANTIA EKIP  |E|23|00342|23|00313|OBSEDOC|OBSERVACIONES DOCUME|A|030|0|O|        |
 * FICHERO: FDXUGE5.txt
</pre></code>
 * 
 * @see RespuestaTransaccionUge5
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "UGE5",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionUge5.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "BMS")}
)
@Multiformato(formatos = {FormatoUGE5E00.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionUge5 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}