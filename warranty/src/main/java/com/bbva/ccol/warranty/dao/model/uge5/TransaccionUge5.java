package com.bbva.ccol.warranty.dao.model.uge5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>UGE5</code>
 * 
 * @see PeticionTransaccionUge5
 * @see RespuestaTransaccionUge5
 */
@Component(value = "transaccionUge5")
@Profile(value = "prod")
public class TransaccionUge5 implements InvocadorTransaccion<PeticionTransaccionUge5,RespuestaTransaccionUge5> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionUge5 invocar(PeticionTransaccionUge5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionUge5.class, RespuestaTransaccionUge5.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionUge5 invocarCache(PeticionTransaccionUge5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionUge5.class, RespuestaTransaccionUge5.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}