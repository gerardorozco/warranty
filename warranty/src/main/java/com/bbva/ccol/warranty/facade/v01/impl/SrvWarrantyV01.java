package com.bbva.ccol.warranty.facade.v01.impl;

import javax.annotation.Resource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import com.bbva.ccol.warranty.business.dto.DTOIntWarranty;
import com.bbva.ccol.warranty.facade.v01.ISrvWarrantyV01;
import com.bbva.ccol.warranty.facade.v01.dto.Warranty;
import com.bbva.ccol.warranty.facade.v01.mapper.impl.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;

import com.bbva.ccol.warranty.business.ISrvIntWarranty;


@Path("/V01")
@SN(registryID="SNPE1500016",logicalID="warranty")
@VN(vnn="V01")
@Api(value="/warranty/V01",description="SN warranty")
@Produces({ MediaType.APPLICATION_JSON})
@Service

	
public class SrvWarrantyV01 implements ISrvWarrantyV01, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

	private static Log log = LogFactory.getLog(SrvWarrantyV01.class);

	public HttpHeaders httpHeaders;
	
	@Autowired
	BusinessServicesToolKit bussinesToolKit;
	

	public UriInfo uriInfo;
	
	@Override
	public void setUriInfo(UriInfo ui) {
		this.uriInfo = ui;		
	}

	@Override
	public void setHttpHeaders(HttpHeaders httpHeaders) {
		this.httpHeaders = httpHeaders;
	}

	@Autowired
	ISrvIntWarranty srvIntWarranty;

	@Resource(name = "warrantyFacadeMapper")
	private Mapper mapper;

	
	@ApiOperation(value="Service for creating warranty", notes="Service for creating warranty",response=Response.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = "aliasGCE1"),
		@ApiResponse(code = -1, message = "aliasGCE2"),
		@ApiResponse(code = 200, message = "Found Sucessfully", response=Response.class),
		@ApiResponse(code = 500, message = "Technical Error")})
	@POST
	@SMC(registryID="SMCPE1500021",logicalID="createWarranty")
	public Response createWarranty(Warranty warranty) {
		DTOIntWarranty dtoIntWarranty = mapper.dtoIntWarrantyMap(warranty);
        srvIntWarranty.createWarranty(dtoIntWarranty);
		return Response.ok().build();
	}

	

}
