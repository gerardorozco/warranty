package com.bbva.ccol.warranty.webservices.warranty.mapper.impl;

import com.bbva.ccol.commons.rm.utils.converters.DateConverter;
import com.bbva.ccol.warranty.business.dto.DTOIntDocuments;
import com.bbva.ccol.warranty.business.dto.DTOIntWarranty;
import com.bbva.ccol.warranty.webservices.warranty.*;
import com.bbva.ccol.warranty.webservices.warranty.mapper.IMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by Entelgy on 23/08/2016.
 */
@Component(value = "wsMapper")
public class Mapper implements IMapper {



    @Override
    public Warranty mapWarranty(DTOIntWarranty dtoIntWarranty) {
        DateConverter dateConverter = new DateConverter();
        Warranty warranty =  new Warranty();
        warranty.setType(new WarrantyType());
        warranty.getType().setId(dtoIntWarranty.getWarrantyType().getIdstring());
        warranty.getType().setName(dtoIntWarranty.getWarrantyType().getNamestring());
        warranty.setValue(new Value());
        warranty.getValue().setAmount(dtoIntWarranty.getValue().getAmountnumber());
        warranty.setRegistryDate(dateConverter.toXMLGregorianCalendar(dtoIntWarranty.getRegistryDatedate()));
        warranty.setExpeditionDate(dateConverter.toXMLGregorianCalendar(dtoIntWarranty.getExpeditionDatedate()));
        warranty.setExpirationDate(dateConverter.toXMLGregorianCalendar(dtoIntWarranty.getExpirationDatedate()));
        warranty.setWarrantyPlace(new WarrantyPlace());
        warranty.getWarrantyPlace().setCountry(dtoIntWarranty.getWarrantyPlace().getCountrynumber());
        warranty.setInsuranceType(new InsuranceType());
        warranty.getInsuranceType().setId(dtoIntWarranty.getInsuranceType().getIdstring());
        warranty.getInsuranceType().setName(dtoIntWarranty.getInsuranceType().getNamestring());
        warranty.setFeatures(new Features());
        warranty.getFeatures().setId(dtoIntWarranty.getFeatures().getIdstring());
        warranty.getFeatures().setDescription(dtoIntWarranty.getFeatures().getDescriptionstring());
        warranty.setAppraisal(new Appraisal());
        warranty.getAppraisal().setDate(dtoIntWarranty.getAppraisal().getDatestring());
        warranty.getAppraisal().setAppraiser(dtoIntWarranty.getAppraisal().getAppraiserstring());

        Documents documents;

        for (DTOIntDocuments dtoIntDocuments : dtoIntWarranty.getDocuments())
        {
               documents =  new Documents();
               documents.setDocumentId(dtoIntDocuments.getDocumentIdstring());
               documents.setType(dtoIntDocuments.getTypestring());
               documents.setNumber(dtoIntDocuments.getNumberstring());
               documents.setOwner(dtoIntDocuments.getOwnerstring());
               documents.setCreationDate(dateConverter.toXMLGregorianCalendar(dtoIntDocuments.getCreationDate()));
               documents.setExpirationDate(dateConverter.toXMLGregorianCalendar(dtoIntDocuments.getExpirationDatedate()));
               documents.setAssociatedValue(new AssociatedValue());
               documents.getAssociatedValue().setType(dtoIntDocuments.getAssociatedValue().getTypestring());
               documents.getAssociatedValue().setAmount(dtoIntDocuments.getAssociatedValue().getAmountnumber());
               documents.getAssociatedValue().setCurrency(dtoIntDocuments.getAssociatedValue().getCurrencynumber());
               documents.setComments(dtoIntDocuments.getCommentsstring());

               warranty.getDocuments().add(documents);
        }

        return warranty;
    }
}



