package com.bbva.ccol.warranty.facade.v01.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by Entelgy on 22/08/2016.
 */
public class WarrantyPlace {

    private BigInteger countrynumber;

    private String citystring;

    public String getCitystring() {
        return citystring;
    }

    public void setCitystring(String citystring) {
        this.citystring = citystring;
    }

    public BigInteger getCountrynumber() {
        return countrynumber;
    }

    public void setCountrynumber(BigInteger countrynumber) {
        this.countrynumber = countrynumber;
    }
}
