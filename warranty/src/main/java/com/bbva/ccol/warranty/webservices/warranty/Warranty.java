
package com.bbva.ccol.warranty.webservices.warranty;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para warranty complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="warranty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="warrantyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="type" type="{http://warranty}warrantyType" minOccurs="0"/>
 *         &lt;element name="value" type="{http://warranty}value" minOccurs="0"/>
 *         &lt;element name="registryDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="expeditionDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="warrantyPlace" type="{http://warranty}warrantyPlace" minOccurs="0"/>
 *         &lt;element name="insuranceType" type="{http://warranty}insuranceType" minOccurs="0"/>
 *         &lt;element name="features" type="{http://warranty}features" minOccurs="0"/>
 *         &lt;element name="appraisal" type="{http://warranty}appraisal" minOccurs="0"/>
 *         &lt;element name="documents" type="{http://warranty}documents" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="associatedNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="relatedContract" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vehicle" type="{http://warranty}vehicle" minOccurs="0"/>
 *         &lt;element name="property" type="{http://warranty}property" minOccurs="0"/>
 *         &lt;element name="promissoryNote" type="{http://warranty}promissoryNote" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="loan" type="{http://warranty}loan" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "warranty", propOrder = {
    "warrantyId",
    "type",
    "value",
    "registryDate",
    "expeditionDate",
    "expirationDate",
    "warrantyPlace",
    "insuranceType",
    "features",
    "appraisal",
    "documents",
    "code",
    "associatedNumber",
    "relatedContract",
    "description",
    "vehicle",
    "property",
    "promissoryNote",
    "loan"
})
public class Warranty {

    protected String warrantyId;
    protected WarrantyType type;
    protected Value value;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar registryDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expeditionDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    protected WarrantyPlace warrantyPlace;
    protected InsuranceType insuranceType;
    protected Features features;
    protected Appraisal appraisal;
    protected List<Documents> documents;
    protected String code;
    protected String associatedNumber;
    protected String relatedContract;
    protected String description;
    protected Vehicle vehicle;
    protected Property property;
    protected List<PromissoryNote> promissoryNote;
    protected Loan loan;

    /**
     * Obtiene el valor de la propiedad warrantyId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarrantyId() {
        return warrantyId;
    }

    /**
     * Define el valor de la propiedad warrantyId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarrantyId(String value) {
        this.warrantyId = value;
    }

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link WarrantyType }
     *     
     */
    public WarrantyType getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link WarrantyType }
     *     
     */
    public void setType(WarrantyType value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link Value }
     *     
     */
    public Value getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link Value }
     *     
     */
    public void setValue(Value value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad registryDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRegistryDate() {
        return registryDate;
    }

    /**
     * Define el valor de la propiedad registryDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRegistryDate(XMLGregorianCalendar value) {
        this.registryDate = value;
    }

    /**
     * Obtiene el valor de la propiedad expeditionDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpeditionDate() {
        return expeditionDate;
    }

    /**
     * Define el valor de la propiedad expeditionDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpeditionDate(XMLGregorianCalendar value) {
        this.expeditionDate = value;
    }

    /**
     * Obtiene el valor de la propiedad expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define el valor de la propiedad expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad warrantyPlace.
     * 
     * @return
     *     possible object is
     *     {@link WarrantyPlace }
     *     
     */
    public WarrantyPlace getWarrantyPlace() {
        return warrantyPlace;
    }

    /**
     * Define el valor de la propiedad warrantyPlace.
     * 
     * @param value
     *     allowed object is
     *     {@link WarrantyPlace }
     *     
     */
    public void setWarrantyPlace(WarrantyPlace value) {
        this.warrantyPlace = value;
    }

    /**
     * Obtiene el valor de la propiedad insuranceType.
     * 
     * @return
     *     possible object is
     *     {@link InsuranceType }
     *     
     */
    public InsuranceType getInsuranceType() {
        return insuranceType;
    }

    /**
     * Define el valor de la propiedad insuranceType.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuranceType }
     *     
     */
    public void setInsuranceType(InsuranceType value) {
        this.insuranceType = value;
    }

    /**
     * Obtiene el valor de la propiedad features.
     * 
     * @return
     *     possible object is
     *     {@link Features }
     *     
     */
    public Features getFeatures() {
        return features;
    }

    /**
     * Define el valor de la propiedad features.
     * 
     * @param value
     *     allowed object is
     *     {@link Features }
     *     
     */
    public void setFeatures(Features value) {
        this.features = value;
    }

    /**
     * Obtiene el valor de la propiedad appraisal.
     * 
     * @return
     *     possible object is
     *     {@link Appraisal }
     *     
     */
    public Appraisal getAppraisal() {
        return appraisal;
    }

    /**
     * Define el valor de la propiedad appraisal.
     * 
     * @param value
     *     allowed object is
     *     {@link Appraisal }
     *     
     */
    public void setAppraisal(Appraisal value) {
        this.appraisal = value;
    }

    /**
     * Gets the value of the documents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocuments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Documents }
     * 
     * 
     */
    public List<Documents> getDocuments() {
        if (documents == null) {
            documents = new ArrayList<Documents>();
        }
        return this.documents;
    }

    /**
     * Obtiene el valor de la propiedad code.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Obtiene el valor de la propiedad associatedNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssociatedNumber() {
        return associatedNumber;
    }

    /**
     * Define el valor de la propiedad associatedNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssociatedNumber(String value) {
        this.associatedNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad relatedContract.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatedContract() {
        return relatedContract;
    }

    /**
     * Define el valor de la propiedad relatedContract.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatedContract(String value) {
        this.relatedContract = value;
    }

    /**
     * Obtiene el valor de la propiedad description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define el valor de la propiedad description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtiene el valor de la propiedad vehicle.
     * 
     * @return
     *     possible object is
     *     {@link Vehicle }
     *     
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Define el valor de la propiedad vehicle.
     * 
     * @param value
     *     allowed object is
     *     {@link Vehicle }
     *     
     */
    public void setVehicle(Vehicle value) {
        this.vehicle = value;
    }

    /**
     * Obtiene el valor de la propiedad property.
     * 
     * @return
     *     possible object is
     *     {@link Property }
     *     
     */
    public Property getProperty() {
        return property;
    }

    /**
     * Define el valor de la propiedad property.
     * 
     * @param value
     *     allowed object is
     *     {@link Property }
     *     
     */
    public void setProperty(Property value) {
        this.property = value;
    }

    /**
     * Gets the value of the promissoryNote property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the promissoryNote property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPromissoryNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PromissoryNote }
     * 
     * 
     */
    public List<PromissoryNote> getPromissoryNote() {
        if (promissoryNote == null) {
            promissoryNote = new ArrayList<PromissoryNote>();
        }
        return this.promissoryNote;
    }

    /**
     * Obtiene el valor de la propiedad loan.
     * 
     * @return
     *     possible object is
     *     {@link Loan }
     *     
     */
    public Loan getLoan() {
        return loan;
    }

    /**
     * Define el valor de la propiedad loan.
     * 
     * @param value
     *     allowed object is
     *     {@link Loan }
     *     
     */
    public void setLoan(Loan value) {
        this.loan = value;
    }

}
