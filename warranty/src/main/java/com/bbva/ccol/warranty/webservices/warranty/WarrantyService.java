/**
 * Created by Apache CXF WadlToJava code generator
**/
package com.bbva.ccol.warranty.webservices.warranty;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/V01")
public interface WarrantyService {

    @POST
    @Consumes("application/json")
    @Produces("application/json;charset=UTF-8")
    Response createWarranty(Warranty warranty);

    @GET
    @Consumes("application/json")
    @Produces("application/json;charset=UTF-8")
    @Path("/{idWarranty}")
    Warranty getWarranty(@PathParam("idWarranty") String idWarranty);

    @PUT
    @Consumes("application/json")
    @Produces("application/json;charset=UTF-8")
    @Path("/{idWarranty}")
    Warranty updateWarranty(@PathParam("idWarranty") String idWarranty);

}