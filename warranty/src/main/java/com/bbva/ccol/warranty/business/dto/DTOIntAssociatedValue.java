package com.bbva.ccol.warranty.business.dto;

import java.math.BigDecimal;

/**
 * Created by Entelgy on 22/08/2016.
 */
public class DTOIntAssociatedValue {

    private String typestring;

    private BigDecimal amountnumber;

    private BigDecimal currencynumber;

    public String getTypestring() {
        return typestring;
    }

    public void setTypestring(String typestring) {
        this.typestring = typestring;
    }

    public BigDecimal getAmountnumber() {
        return amountnumber;
    }

    public void setAmountnumber(BigDecimal amountnumber) {
        this.amountnumber = amountnumber;
    }

    public BigDecimal getCurrencynumber() {
        return currencynumber;
    }

    public void setCurrencynumber(BigDecimal currencynumber) {
        this.currencynumber = currencynumber;
    }
}
