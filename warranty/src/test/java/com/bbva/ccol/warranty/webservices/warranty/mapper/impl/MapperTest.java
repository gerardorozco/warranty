package com.bbva.ccol.warranty.webservices.warranty.mapper.impl;

import com.bbva.ccol.commons.rm.utils.converters.DateConverter;
import com.bbva.ccol.warranty.business.dto.*;
import com.bbva.ccol.warranty.webservices.warranty.Warranty;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.Before;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

public class MapperTest {

    Mapper mapper;

    @Before
    public void init() {
        mapper = new Mapper();
    }

    @Test
    public void mapWarrantyTest()
    {
        DateConverter dateConverter = new DateConverter();

        DTOIntWarranty dtoIntWarranty =  new DTOIntWarranty();
        dtoIntWarranty.setWarrantyType(new DTOIntWarrantyType());
        dtoIntWarranty.getWarrantyType().setIdstring("1235");
        dtoIntWarranty.getWarrantyType().setNamestring("name");
        dtoIntWarranty.setValue(new DTOIntValue());
        dtoIntWarranty.getValue().setAmountnumber(BigDecimal.TEN);
        dtoIntWarranty.setRegistryDatedate(new Date());
        dtoIntWarranty.setExpeditionDatedate(new Date());
        dtoIntWarranty.setExpirationDatedate(new Date());
        dtoIntWarranty.setWarrantyPlace(new DTOIntWarrantyPlace());
        dtoIntWarranty.getWarrantyPlace().setCountrynumber(BigInteger.TEN);
        dtoIntWarranty.setInsuranceType(new DTOIntInsuranceType());
        dtoIntWarranty.getInsuranceType().setIdstring("1111");
        dtoIntWarranty.getInsuranceType().setNamestring("name");
        dtoIntWarranty.setFeatures(new DTOIntFeatures());
        dtoIntWarranty.getFeatures().setDescriptionstring("desc");
        dtoIntWarranty.setAppraisal(new DTOIntAppraisal());
        dtoIntWarranty.getAppraisal().setDatestring(new Date().toString());
        dtoIntWarranty.getAppraisal().setAppraiserstring("123");
        dtoIntWarranty.setDocuments(new ArrayList<DTOIntDocuments>());

        DTOIntDocuments dtoIntDocuments =  new DTOIntDocuments();
        dtoIntDocuments.setDocumentIdstring("12344");
        dtoIntDocuments.setTypestring("0");
        dtoIntDocuments.setNumberstring("666");
        dtoIntDocuments.setOwnerstring("gerardo");
        dtoIntDocuments.setCreationDate(new Date());
        dtoIntDocuments.setExpirationDatedate(new Date());
        dtoIntDocuments.setAssociatedValue(new DTOIntAssociatedValue());
        dtoIntDocuments.getAssociatedValue().setTypestring("555");
        dtoIntDocuments.getAssociatedValue().setAmountnumber(BigDecimal.TEN);
        dtoIntDocuments.getAssociatedValue().setCurrencynumber(BigDecimal.TEN);
        dtoIntDocuments.setCommentsstring("Comments");

        dtoIntWarranty.getDocuments().add(dtoIntDocuments);

        Warranty warranty =  mapper.mapWarranty(dtoIntWarranty);

        Assert.assertNotNull(warranty);
        Assert.assertEquals(warranty.getType().getId(),dtoIntWarranty.getWarrantyType().getIdstring());
        Assert.assertEquals(warranty.getType().getName(),dtoIntWarranty.getWarrantyType().getNamestring());
        Assert.assertEquals(warranty.getValue().getAmount(),dtoIntWarranty.getValue().getAmountnumber());
        Assert.assertEquals(warranty.getRegistryDate(),dateConverter.toXMLGregorianCalendar(dtoIntWarranty.getRegistryDatedate()));
        Assert.assertEquals(warranty.getExpeditionDate(),dateConverter.toXMLGregorianCalendar(dtoIntWarranty.getExpeditionDatedate()));
        Assert.assertEquals(warranty.getExpirationDate(),dateConverter.toXMLGregorianCalendar(dtoIntWarranty.getExpirationDatedate()));
        Assert.assertEquals(warranty.getWarrantyPlace().getCountry(),dtoIntWarranty.getWarrantyPlace().getCountrynumber());
        Assert.assertEquals(warranty.getInsuranceType().getId(),dtoIntWarranty.getInsuranceType().getIdstring());
        Assert.assertEquals(warranty.getInsuranceType().getName(),dtoIntWarranty.getInsuranceType().getNamestring());
        Assert.assertEquals(warranty.getFeatures().getId(),dtoIntWarranty.getFeatures().getIdstring());
        Assert.assertEquals(warranty.getFeatures().getDescription(),dtoIntWarranty.getFeatures().getDescriptionstring());
        Assert.assertEquals(warranty.getAppraisal().getDate(),dtoIntWarranty.getAppraisal().getDatestring());
        Assert.assertEquals(warranty.getAppraisal().getAppraiser(),dtoIntWarranty.getAppraisal().getAppraiserstring());
        Assert.assertEquals(warranty.getDocuments().get(0).getDocumentId(),dtoIntWarranty.getDocuments().get(0).getDocumentIdstring());
        Assert.assertEquals(warranty.getDocuments().get(0).getType(),dtoIntWarranty.getDocuments().get(0).getTypestring());
        Assert.assertEquals(warranty.getDocuments().get(0).getNumber(),dtoIntWarranty.getDocuments().get(0).getNumberstring());
        Assert.assertEquals(warranty.getDocuments().get(0).getOwner(),dtoIntWarranty.getDocuments().get(0).getOwnerstring());
        Assert.assertEquals(warranty.getDocuments().get(0).getCreationDate(),dateConverter.toXMLGregorianCalendar(dtoIntWarranty.getDocuments().get(0).getCreationDate()));
        Assert.assertEquals(warranty.getDocuments().get(0).getExpirationDate(),dateConverter.toXMLGregorianCalendar(dtoIntWarranty.getDocuments().get(0).getExpirationDatedate()));
        Assert.assertEquals(warranty.getDocuments().get(0).getAssociatedValue().getType(),dtoIntWarranty.getDocuments().get(0).getAssociatedValue().getTypestring());
        Assert.assertEquals(warranty.getDocuments().get(0).getAssociatedValue().getAmount(),dtoIntWarranty.getDocuments().get(0).getAssociatedValue().getAmountnumber());
        Assert.assertEquals(warranty.getDocuments().get(0).getAssociatedValue().getCurrency(),dtoIntWarranty.getDocuments().get(0).getAssociatedValue().getCurrencynumber());
        Assert.assertEquals(warranty.getDocuments().get(0).getComments(),dtoIntWarranty.getDocuments().get(0).getCommentsstring());


    }
}