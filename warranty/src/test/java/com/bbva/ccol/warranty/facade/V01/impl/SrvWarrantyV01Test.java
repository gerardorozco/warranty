package com.bbva.ccol.warranty.facade.V01.impl;

import com.bbva.ccol.warranty.facade.v01.dto.*;


import com.bbva.ccol.warranty.facade.v01.impl.SrvWarrantyV01;
import com.bbva.jee.arq.spring.core.servicing.test.BusinessServiceTestContextLoader;
import com.bbva.jee.arq.spring.core.servicing.test.MockInvocationContextTestExecutionListener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = BusinessServiceTestContextLoader.class, locations = {
        "classpath*:/META-INF/spring/applicationContext-*.xml", "classpath:/META-INF/spring/business-service.xml",
        "classpath:/META-INF/spring/business-service-test.xml" })
@TestExecutionListeners(listeners = { MockInvocationContextTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class })
@ActiveProfiles("dev")
public class SrvWarrantyV01Test {

    @Autowired
    private SrvWarrantyV01 srvWarrantyV01;

    @Test
    public void WarrantyTestOk()
    {
        Warranty warranty =  new Warranty();
        warranty.setWarrantyType(new WarrantyType());
        warranty.getWarrantyType().setIdstring("123");
        warranty.getWarrantyType().setNamestring("name");
        warranty.setValue(new Value());
        warranty.getValue().setAmountnumber(BigDecimal.ONE);
        warranty.setRegistryDatedate(new Date());
        warranty.setExpeditionDatedate(new Date());
        warranty.setExpirationDatedate(new Date());
        warranty.setWarrantyPlace(new WarrantyPlace());
        warranty.getWarrantyPlace().setCountrynumber(BigInteger.ONE);
        warranty.setInsuranceType(new InsuranceType());
        warranty.getInsuranceType().setIdstring("123");
        warranty.getInsuranceType().setNamestring("name");
        warranty.setFeatures(new Features());
        warranty.getFeatures().setIdstring("123");
        warranty.getFeatures().setDescriptionstring("Description");
        warranty.setAppraisal(new Appraisal());
        warranty.getAppraisal().setDatestring(new Date().toString());
        warranty.getAppraisal().setAppraiserstring("123");

        Documents documents =  new Documents();
        documents.setDocumentIdstring("123");
        documents.setTypestring("0");
        documents.setNumberstring("123");
        documents.setOwnerstring("owner");
        documents.setCreationDate(new Date());
        documents.setExpirationDatedate(new Date());
        documents.setAssociatedValue(new AssociatedValue());
        documents.getAssociatedValue().setTypestring("0");
        documents.getAssociatedValue().setAmountnumber(BigDecimal.ONE);
        documents.getAssociatedValue().setCurrencynumber(BigDecimal.TEN);
        documents.setCommentsstring("comment");

        warranty.setDocuments(new ArrayList<Documents>());
        warranty.getDocuments().add(documents);

        srvWarrantyV01.createWarranty(warranty);
    }

}