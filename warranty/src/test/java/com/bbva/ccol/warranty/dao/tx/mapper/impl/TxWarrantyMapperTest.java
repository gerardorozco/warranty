package com.bbva.ccol.warranty.dao.tx.mapper.impl;

import com.bbva.ccol.warranty.business.dto.*;
import com.bbva.ccol.warranty.dao.model.uge5.FormatoUGE5E00;
import com.bbva.ccol.warranty.dao.tx.TxWarranty;
import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

public class TxWarrantyMapperTest  {

    @Resource(name = "txWarrantyMapper")
    private TxWarrantyMapper mapper;

    @Before
    public void setUp() {
        mapper = new TxWarrantyMapper();
    }

    @Test
    public void mapDtoInToRequestFormatTest()
    {
       DTOIntWarranty dtoIntWarranty =  new DTOIntWarranty();
       dtoIntWarranty.setWarrantyType(new DTOIntWarrantyType());
       dtoIntWarranty.getWarrantyType().setIdstring("1235");
       dtoIntWarranty.getWarrantyType().setNamestring("name");
       dtoIntWarranty.setValue(new DTOIntValue());
       dtoIntWarranty.getValue().setAmountnumber(BigDecimal.TEN);
       dtoIntWarranty.setRegistryDatedate(new Date());
       dtoIntWarranty.setExpeditionDatedate(new Date());
       dtoIntWarranty.setExpirationDatedate(new Date());
       dtoIntWarranty.setWarrantyPlace(new DTOIntWarrantyPlace());
       dtoIntWarranty.getWarrantyPlace().setCountrynumber(BigInteger.TEN);
       dtoIntWarranty.setInsuranceType(new DTOIntInsuranceType());
       dtoIntWarranty.getInsuranceType().setIdstring("1111");
       dtoIntWarranty.getInsuranceType().setNamestring("name");
       dtoIntWarranty.setFeatures(new DTOIntFeatures());
       dtoIntWarranty.getFeatures().setDescriptionstring("desc");
       dtoIntWarranty.setAppraisal(new DTOIntAppraisal());
       dtoIntWarranty.getAppraisal().setDatestring(new Date().toString());
       dtoIntWarranty.getAppraisal().setAppraiserstring("123");
       dtoIntWarranty.setDocuments(new ArrayList<DTOIntDocuments>());

       DTOIntDocuments dtoIntDocuments =  new DTOIntDocuments();
       dtoIntDocuments.setDocumentIdstring("12344");
       dtoIntDocuments.setTypestring("0");
       dtoIntDocuments.setNumberstring("666");
       dtoIntDocuments.setOwnerstring("gerardo");
       dtoIntDocuments.setCreationDate(new Date());
       dtoIntDocuments.setExpirationDatedate(new Date());
       dtoIntDocuments.setAssociatedValue(new DTOIntAssociatedValue());
       dtoIntDocuments.getAssociatedValue().setTypestring("555");
       dtoIntDocuments.getAssociatedValue().setAmountnumber(BigDecimal.TEN);
       dtoIntDocuments.getAssociatedValue().setCurrencynumber(BigDecimal.TEN);
       dtoIntDocuments.setCommentsstring("Comments");

       dtoIntWarranty.getDocuments().add(dtoIntDocuments);

       FormatoUGE5E00 formatoUGE5E00 =  mapper.mapDtoInToRequestFormat(dtoIntWarranty);

        Assert.assertNotNull(formatoUGE5E00);
        Assert.assertEquals(dtoIntWarranty.getWarrantyType().getIdstring(),formatoUGE5E00.getTigarid());
        Assert.assertEquals(dtoIntWarranty.getWarrantyType().getNamestring(),formatoUGE5E00.getNomgara());
        Assert.assertEquals(dtoIntWarranty.getValue().getAmountnumber().toString(),formatoUGE5E00.getValgara());
        Assert.assertEquals(dtoIntWarranty.getRegistryDatedate().toString(),formatoUGE5E00.getFecrega());
        Assert.assertEquals(dtoIntWarranty.getExpeditionDatedate().toString(),formatoUGE5E00.getFecexpd());
        Assert.assertEquals(dtoIntWarranty.getExpirationDatedate().toString(),formatoUGE5E00.getFecvend());
        Assert.assertEquals(dtoIntWarranty.getWarrantyPlace().getCountrynumber().toString(),formatoUGE5E00.getPaisgar());
        Assert.assertEquals(dtoIntWarranty.getInsuranceType().getIdstring(),formatoUGE5E00.getGaidres());
        Assert.assertEquals(dtoIntWarranty.getInsuranceType().getNamestring(),formatoUGE5E00.getGanores());
        Assert.assertEquals(dtoIntWarranty.getFeatures().getIdstring(),formatoUGE5E00.getTifunga());
        Assert.assertEquals(dtoIntWarranty.getFeatures().getDescriptionstring(),formatoUGE5E00.getDefunga());
        Assert.assertEquals(dtoIntWarranty.getAppraisal().getDatestring(),formatoUGE5E00.getFectime());
        Assert.assertEquals(dtoIntWarranty.getAppraisal().getAppraiserstring(),formatoUGE5E00.getNomevag());
        Assert.assertEquals(dtoIntWarranty.getDocuments().get(0).getDocumentIdstring(),formatoUGE5E00.getIdunasd());
        Assert.assertEquals(dtoIntWarranty.getDocuments().get(0).getTypestring(),formatoUGE5E00.getTipdocu());
        Assert.assertEquals(dtoIntWarranty.getDocuments().get(0).getNumberstring(),formatoUGE5E00.getNumdocu());
        Assert.assertEquals(dtoIntWarranty.getDocuments().get(0).getOwnerstring(),formatoUGE5E00.getProdocu());
        Assert.assertEquals(dtoIntWarranty.getDocuments().get(0).getCreationDate().toString(),formatoUGE5E00.getFeccred());
        Assert.assertEquals(dtoIntWarranty.getDocuments().get(0).getExpirationDatedate().toString(),formatoUGE5E00.getFecvedo());
        Assert.assertEquals(dtoIntWarranty.getDocuments().get(0).getAssociatedValue().getTypestring(),formatoUGE5E00.getTivaasd());
        Assert.assertEquals(dtoIntWarranty.getDocuments().get(0).getAssociatedValue().getAmountnumber().toString(),formatoUGE5E00.getCavaasd());
        Assert.assertEquals(dtoIntWarranty.getDocuments().get(0).getAssociatedValue().getCurrencynumber().toString(),formatoUGE5E00.getMonasd());
        Assert.assertEquals(dtoIntWarranty.getDocuments().get(0).getCommentsstring(),formatoUGE5E00.getObsedoc());

    }
}



















