package com.bbva.ccol.warranty.dao.tx.mapper.impl;

import com.bbva.ccol.warranty.business.dto.DTOIntWarranty;
import com.bbva.ccol.warranty.dao.model.uge5.FormatoUGE5E00;
import com.bbva.ccol.warranty.dao.tx.mapper.ITxWarrantyMapper;
import org.springframework.stereotype.Component;

/**
 * Created by Entelgy on 23/08/2016.
 */

@Component(value = "txWarrantyMapper")
public class TxWarrantyMapper implements ITxWarrantyMapper {
    @Override
    public FormatoUGE5E00 mapDtoInToRequestFormat(DTOIntWarranty dtoIn) {

        FormatoUGE5E00 formatoUGE5E00 =  new FormatoUGE5E00();

        formatoUGE5E00.setTigarid(dtoIn.getWarrantyType().getIdstring());
        formatoUGE5E00.setNomgara(dtoIn.getWarrantyType().getNamestring());
        formatoUGE5E00.setValgara(dtoIn.getValue().getAmountnumber().toString());
        formatoUGE5E00.setFecrega(dtoIn.getRegistryDatedate().toString());
        formatoUGE5E00.setFecexpd(dtoIn.getExpeditionDatedate().toString());
        formatoUGE5E00.setFecvend(dtoIn.getExpirationDatedate().toString());
        formatoUGE5E00.setPaisgar(dtoIn.getWarrantyPlace().getCountrynumber().toString());
        formatoUGE5E00.setGaidres(dtoIn.getInsuranceType().getIdstring());
        formatoUGE5E00.setGanores(dtoIn.getInsuranceType().getNamestring());
        formatoUGE5E00.setTifunga(dtoIn.getFeatures().getIdstring());
        formatoUGE5E00.setDefunga(dtoIn.getFeatures().getDescriptionstring());
        formatoUGE5E00.setFectime(dtoIn.getAppraisal().getDatestring());
        formatoUGE5E00.setNomevag(dtoIn.getAppraisal().getAppraiserstring());
        formatoUGE5E00.setIdunasd(dtoIn.getDocuments().get(0).getDocumentIdstring());
        formatoUGE5E00.setTipdocu(dtoIn.getDocuments().get(0).getTypestring());
        formatoUGE5E00.setNumdocu(dtoIn.getDocuments().get(0).getNumberstring());
        formatoUGE5E00.setProdocu(dtoIn.getDocuments().get(0).getOwnerstring());
        formatoUGE5E00.setFeccred(dtoIn.getDocuments().get(0).getCreationDate().toString());
        formatoUGE5E00.setFecvedo(dtoIn.getDocuments().get(0).getExpirationDatedate().toString());
        formatoUGE5E00.setTivaasd(dtoIn.getDocuments().get(0).getAssociatedValue().getTypestring());
        formatoUGE5E00.setCavaasd(dtoIn.getDocuments().get(0).getAssociatedValue().getAmountnumber().toString());
        formatoUGE5E00.setMonasd(dtoIn.getDocuments().get(0).getAssociatedValue().getCurrencynumber().toString());
        formatoUGE5E00.setObsedoc(dtoIn.getDocuments().get(0).getCommentsstring());

        return formatoUGE5E00;
    }
}
