package com.bbva.ccol.warranty.dao;

import com.bbva.ccol.warranty.business.dto.DTOIntWarranty;


public interface IWarrantyDAO {

  void createWarranty(DTOIntWarranty dtoIntWarranty);
	
}

