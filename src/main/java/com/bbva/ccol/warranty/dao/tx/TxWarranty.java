package com.bbva.ccol.warranty.dao.tx;

import com.bbva.ccol.commons.rm.utils.tx.impl.SimpleNoResultBbvaTransaction;
import com.bbva.ccol.warranty.business.dto.DTOIntWarranty;
import com.bbva.ccol.warranty.dao.model.uge5.FormatoUGE5E00;
import com.bbva.ccol.warranty.dao.model.uge5.PeticionTransaccionUge5;
import com.bbva.ccol.warranty.dao.model.uge5.RespuestaTransaccionUge5;
import com.bbva.ccol.warranty.dao.tx.mapper.ITxWarrantyMapper;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by Entelgy on 23/08/2016.
 */
@Component(value = "txWarranty")
public class TxWarranty extends SimpleNoResultBbvaTransaction<DTOIntWarranty, FormatoUGE5E00, DTOIntWarranty> {

    @Resource(name = "txWarrantyMapper")
    ITxWarrantyMapper mapper;

    @Resource(name = "transaccionUge5")
    private InvocadorTransaccion<PeticionTransaccionUge5,RespuestaTransaccionUge5> transaccionUge5;

    @Override
    protected FormatoUGE5E00 mapDtoInToRequestFormat(DTOIntWarranty dtoIn) {
        return mapper.mapDtoInToRequestFormat(dtoIn);
    }

    @Override
    protected InvocadorTransaccion<?, ?> getTransaction() {
        return transaccionUge5;
    }
}
