package com.bbva.ccol.warranty.dao.model.uge5.mock;

import com.bbva.ccol.warranty.dao.model.uge5.PeticionTransaccionUge5;
import com.bbva.ccol.warranty.dao.model.uge5.RespuestaTransaccionUge5;
import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>UGE4</code>
 *
 * @see com.bbva.ccol.warranty.dao.model.uge5.PeticionTransaccionUge5
 * @see com.bbva.ccol.warranty.dao.model.uge5.RespuestaTransaccionUge5
 */
@Component(value = "transaccionUge5")
@Profile(value = "dev")
public class TransaccionUge5Mock implements InvocadorTransaccion<PeticionTransaccionUge5, RespuestaTransaccionUge5> {


    @Override
    public RespuestaTransaccionUge5 invocar(PeticionTransaccionUge5 transaccion) throws ExcepcionTransaccion {
        RespuestaTransaccionUge5 respuestaTransaccionUge5 = new RespuestaTransaccionUge5();

        return respuestaTransaccionUge5;
    }

    @Override
    public RespuestaTransaccionUge5 invocarCache(PeticionTransaccionUge5 transaccion) throws ExcepcionTransaccion {
        return null;
    }

    @Override
    public void vaciarCache() {
    }
}