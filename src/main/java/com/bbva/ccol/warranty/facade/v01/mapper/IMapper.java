package com.bbva.ccol.warranty.facade.v01.mapper;

import com.bbva.ccol.warranty.business.dto.DTOIntWarranty;
import com.bbva.ccol.warranty.facade.v01.dto.Warranty;

/**
 * Created by Entelgy on 23/08/2016.
 */
public interface IMapper {

    DTOIntWarranty dtoIntWarrantyMap(Warranty warranty);
}
