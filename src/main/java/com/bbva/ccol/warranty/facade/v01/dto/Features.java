package com.bbva.ccol.warranty.facade.v01.dto;

/**
 * Created by Entelgy on 22/08/2016.
 */
public class Features {

    private String idstring;

    private String descriptionstring;

    public String getDescriptionstring() {
        return descriptionstring;
    }

    public void setDescriptionstring(String descriptionstring) {
        this.descriptionstring = descriptionstring;
    }

    public String getIdstring() {
        return idstring;
    }

    public void setIdstring(String idstring) {
        this.idstring = idstring;
    }

}
