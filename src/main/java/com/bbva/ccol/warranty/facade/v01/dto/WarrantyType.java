package com.bbva.ccol.warranty.facade.v01.dto;

/**
 * Created by Entelgy on 22/08/2016.
 */
public class WarrantyType {

    private String idstring;

    private String namestring;

    public String getNamestring() {
        return namestring;
    }

    public void setNamestring(String namestring) {
        this.namestring = namestring;
    }

    public String getIdstring() {
        return idstring;
    }

    public void setIdstring(String idstring) {
        this.idstring = idstring;
    }

}
