package com.bbva.ccol.warranty.facade.v01.mapper.impl;

import com.bbva.ccol.warranty.business.dto.*;
import com.bbva.ccol.warranty.facade.v01.dto.Documents;
import com.bbva.ccol.warranty.facade.v01.dto.Warranty;
import com.bbva.ccol.warranty.facade.v01.mapper.IMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(value = "warrantyFacadeMapper")
public class Mapper implements IMapper {

		
 public DTOIntWarranty dtoIntWarrantyMap(Warranty warranty) {

	    DTOIntWarranty dtoIntWarranty = new DTOIntWarranty();
        dtoIntWarranty.setWarrantyType(new DTOIntWarrantyType());
	    dtoIntWarranty.getWarrantyType().setIdstring(warranty.getWarrantyType().getIdstring());
	    dtoIntWarranty.getWarrantyType().setNamestring(warranty.getWarrantyType().getNamestring());
	    dtoIntWarranty.setValue(new DTOIntValue());
	    dtoIntWarranty.getValue().setAmountnumber(warranty.getValue().getAmountnumber());
	    dtoIntWarranty.setRegistryDatedate(warranty.getRegistryDatedate());
	    dtoIntWarranty.setExpeditionDatedate(warranty.getExpeditionDatedate());
	    dtoIntWarranty.setExpirationDatedate(warranty.getExpirationDatedate());
	    dtoIntWarranty.setWarrantyPlace(new DTOIntWarrantyPlace());
	    dtoIntWarranty.getWarrantyPlace().setCountrynumber(warranty.getWarrantyPlace().getCountrynumber());
	    dtoIntWarranty.setInsuranceType(new DTOIntInsuranceType());
	    dtoIntWarranty.getInsuranceType().setIdstring(warranty.getInsuranceType().getIdstring());
	    dtoIntWarranty.getInsuranceType().setNamestring(warranty.getInsuranceType().getNamestring());
	    dtoIntWarranty.setFeatures(new DTOIntFeatures());
	    dtoIntWarranty.getFeatures().setIdstring(warranty.getFeatures().getIdstring());
	    dtoIntWarranty.getFeatures().setDescriptionstring(warranty.getFeatures().getDescriptionstring());
	    dtoIntWarranty.setAppraisal(new DTOIntAppraisal());
	    dtoIntWarranty.getAppraisal().setDatestring(warranty.getAppraisal().getDatestring());
	    dtoIntWarranty.getAppraisal().setAppraiserstring(warranty.getAppraisal().getAppraiserstring());

	    List<DTOIntDocuments> dtoIntDocumentsList = new ArrayList<DTOIntDocuments>();
	    DTOIntDocuments dtoIntDocuments;

		  for (Documents documents : warranty.getDocuments())
		  {
			dtoIntDocuments =  new DTOIntDocuments();
			dtoIntDocuments.setDocumentIdstring(documents.getDocumentIdstring());
			dtoIntDocuments.setTypestring(documents.getTypestring());
			dtoIntDocuments.setNumberstring(documents.getNumberstring());
			dtoIntDocuments.setOwnerstring(documents.getOwnerstring());
			dtoIntDocuments.setCreationDate(documents.getCreationDate());
			dtoIntDocuments.setExpirationDatedate(documents.getExpirationDatedate());
	        dtoIntDocuments.setAssociatedValue(new DTOIntAssociatedValue());
	        dtoIntDocuments.getAssociatedValue().setTypestring(documents.getAssociatedValue().getTypestring());
		    dtoIntDocuments.getAssociatedValue().setAmountnumber(documents.getAssociatedValue().getAmountnumber());
			dtoIntDocuments.getAssociatedValue().setCurrencynumber(documents.getAssociatedValue().getCurrencynumber());
			dtoIntDocuments.setCommentsstring(documents.getCommentsstring());
			dtoIntDocumentsList.add(dtoIntDocuments);
		  }

	     dtoIntWarranty.setDocuments(dtoIntDocumentsList);

	    return dtoIntWarranty;
 }
	
	
}

