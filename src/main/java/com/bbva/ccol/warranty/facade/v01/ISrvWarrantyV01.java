package com.bbva.ccol.warranty.facade.v01;

import java.util.List;
import javax.ws.rs.core.Response;

import com.bbva.ccol.warranty.facade.v01.dto.Warranty;


public interface ISrvWarrantyV01 {
 	public Response createWarranty(Warranty warranty);

	
}