package com.bbva.ccol.warranty.business.dto;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by Entelgy on 22/08/2016.
 */
public class DTOIntWarrantyPlace {

    private BigInteger countrynumber;

    private String citystring;

    public String getCitystring() {
        return citystring;
    }

    public void setCitystring(String citystring) {
        this.citystring = citystring;
    }

    public BigInteger getCountrynumber() {
        return countrynumber;
    }

    public void setCountrynumber(BigInteger countrynumber) {
        this.countrynumber = countrynumber;
    }
}
