package com.bbva.ccol.warranty.business.impl;

import com.bbva.ccol.commons.rm.utils.errors.EnumError;
import com.bbva.ccol.warranty.dao.IWarrantyDAO;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;
import com.bbva.ccol.warranty.business.dto.DTOIntWarranty;
import com.bbva.ccol.warranty.business.ISrvIntWarranty;
import javax.annotation.Resource;
import static com.bbva.ccol.commons.rm.utils.CallSequenceMessageEnum.SERVICE_REQUEST;
import static com.bbva.ccol.commons.rm.utils.LogUtils.buildLogMessage;


@Service(value = "srvIntWarranty")
public class SrvIntWarranty implements ISrvIntWarranty {
	
	private static Log log = LogFactory.getLog(SrvIntWarranty.class);

	@Autowired
	BusinessServicesToolKit bussinesToolKit;

	@Resource(name = "warrantyDAO")
	IWarrantyDAO dao;
	
	@Override
	 public void createWarranty(DTOIntWarranty dtoIntWarranty) {

		log.debug(buildLogMessage(SERVICE_REQUEST, dtoIntWarranty));

		if (dtoIntWarranty == null)
			throw new BusinessServiceException(EnumError.MANDATORY_PARAMETERES_MISSING.getAlias());

		dao.createWarranty(dtoIntWarranty);

	}

	

}
