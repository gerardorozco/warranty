package com.bbva.ccol.warranty.business.dto;

/**
 * Created by Entelgy on 22/08/2016.
 */
public class DTOIntFeatures {

    private String idstring;

    private String descriptionstring;

    public String getDescriptionstring() {
        return descriptionstring;
    }

    public void setDescriptionstring(String descriptionstring) {
        this.descriptionstring = descriptionstring;
    }

    public String getIdstring() {
        return idstring;
    }

    public void setIdstring(String idstring) {
        this.idstring = idstring;
    }
}
