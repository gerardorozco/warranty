package com.bbva.ccol.warranty.business.dto;

import java.util.Date;

/**
 * Created by Entelgy on 22/08/2016.
 */
public class DTOIntDocuments {

    private String documentIdstring;

    private String typestring;

    private String numberstring;

    private String ownerstring;

    private Date creationDate;

    private Date expirationDatedate;

    private String commentsstring;

    private  DTOIntAssociatedValue associatedValue;

    public String getDocumentIdstring() {
        return documentIdstring;
    }

    public void setDocumentIdstring(String documentIdstring) {
        this.documentIdstring = documentIdstring;
    }

    public String getTypestring() {
        return typestring;
    }

    public void setTypestring(String typestring) {
        this.typestring = typestring;
    }


    public String getNumberstring() {
        return numberstring;
    }

    public void setNumberstring(String numberstring) {
        this.numberstring = numberstring;
    }


    public String getOwnerstring() {
        return ownerstring;
    }

    public void setOwnerstring(String ownerstring) {
        this.ownerstring = ownerstring;
    }


    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


    public Date getExpirationDatedate() {
        return expirationDatedate;
    }

    public void setExpirationDatedate(Date expirationDatedate) {
        this.expirationDatedate = expirationDatedate;
    }


    public DTOIntAssociatedValue getAssociatedValue() {
        return associatedValue;
    }

    public void setAssociatedValue( DTOIntAssociatedValue associatedValue) {
        this.associatedValue = associatedValue;
    }

    public String getCommentsstring() {
        return commentsstring;
    }

    public void setCommentsstring(String commentsstring) {
        this.commentsstring = commentsstring;
    }

}
