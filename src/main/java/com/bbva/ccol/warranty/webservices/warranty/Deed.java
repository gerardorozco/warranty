
package com.bbva.ccol.warranty.webservices.warranty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para deed complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="deed">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="notaryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cityRegistry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="stratum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deed", propOrder = {
    "notaryCode",
    "cityRegistry",
    "stratum"
})
public class Deed {

    protected String notaryCode;
    protected String cityRegistry;
    protected String stratum;

    /**
     * Obtiene el valor de la propiedad notaryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotaryCode() {
        return notaryCode;
    }

    /**
     * Define el valor de la propiedad notaryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotaryCode(String value) {
        this.notaryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad cityRegistry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityRegistry() {
        return cityRegistry;
    }

    /**
     * Define el valor de la propiedad cityRegistry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityRegistry(String value) {
        this.cityRegistry = value;
    }

    /**
     * Obtiene el valor de la propiedad stratum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStratum() {
        return stratum;
    }

    /**
     * Define el valor de la propiedad stratum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStratum(String value) {
        this.stratum = value;
    }

}
