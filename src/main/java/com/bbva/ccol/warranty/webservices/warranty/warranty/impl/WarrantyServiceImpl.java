package com.bbva.ccol.warranty.webservices.warranty.warranty.impl;

import com.bbva.ccol.commons.rm.utils.webService.AbstractBbvaRestService;
import com.bbva.ccol.commons.rm.utils.webService.RestService;
import com.bbva.ccol.warranty.webservices.warranty.Warranty;
import com.bbva.ccol.warranty.webservices.warranty.WarrantyService;
import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import javax.ws.rs.core.Response;

/**
 * Created by Entelgy on 23/08/2016.
 */
@RestService(value = "warrantyService")
public class WarrantyServiceImpl extends AbstractBbvaRestService implements WarrantyService {

    @Value("#{rest['rest.base.url']}")
     protected String URL_BASE;


    @Override
    public Response createWarranty(Warranty warranty) {

        Response response = null;

        try
        {
           WebClient wc = getJsonWebClient(URL_BASE);
           response = wc.post(warranty);
        }
        catch (Exception e)
        {
            String ex = e.getMessage();
            throw new RestClientException("Servicio no disponible");
        }

        return response;
    }

    @Override
    public Warranty getWarranty(String idWarranty) {
        return null;
    }

    @Override
    public Warranty updateWarranty(String idWarranty) {
        return null;
    }
}
