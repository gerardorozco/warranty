package com.bbva.ccol.warranty.webservices.warranty.mapper;

import com.bbva.ccol.warranty.business.dto.DTOIntWarranty;
import com.bbva.ccol.warranty.webservices.warranty.Warranty;

/**
 * Created by Entelgy on 23/08/2016.
 */
public interface IMapper {

    Warranty mapWarranty(DTOIntWarranty dtoIntWarranty);

}
