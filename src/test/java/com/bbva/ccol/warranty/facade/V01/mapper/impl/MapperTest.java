package com.bbva.ccol.warranty.facade.V01.mapper.impl;

import com.bbva.ccol.warranty.business.dto.DTOIntWarranty;
import com.bbva.ccol.warranty.facade.v01.dto.*;
import com.bbva.ccol.warranty.facade.v01.mapper.impl.Mapper;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

public class MapperTest {

    private Mapper mapper;

    @Before
    public void init() {
        mapper = new Mapper();
    }

    @Test
    public void dtoIntWarrantyMapTest()
    {
        Warranty warranty =  new Warranty();
        warranty.setWarrantyType(new WarrantyType());
        warranty.getWarrantyType().setIdstring("123");
        warranty.getWarrantyType().setNamestring("name");
        warranty.setValue(new Value());
        warranty.getValue().setAmountnumber(BigDecimal.ONE);
        warranty.setRegistryDatedate(new Date());
        warranty.setExpeditionDatedate(new Date());
        warranty.setExpirationDatedate(new Date());
        warranty.setWarrantyPlace(new WarrantyPlace());
        warranty.getWarrantyPlace().setCountrynumber(BigInteger.ONE);
        warranty.setInsuranceType(new InsuranceType());
        warranty.getInsuranceType().setIdstring("123");
        warranty.getInsuranceType().setNamestring("name");
        warranty.setFeatures(new Features());
        warranty.getFeatures().setIdstring("123");
        warranty.getFeatures().setDescriptionstring("Description");
        warranty.setAppraisal(new Appraisal());
        warranty.getAppraisal().setDatestring(new Date().toString());
        warranty.getAppraisal().setAppraiserstring("123");

        Documents documents =  new Documents();
        documents.setDocumentIdstring("123");
        documents.setTypestring("0");
        documents.setNumberstring("123");
        documents.setOwnerstring("owner");
        documents.setCreationDate(new Date());
        documents.setExpirationDatedate(new Date());
        documents.setAssociatedValue(new AssociatedValue());
        documents.getAssociatedValue().setTypestring("0");
        documents.getAssociatedValue().setAmountnumber(BigDecimal.ONE);
        documents.getAssociatedValue().setCurrencynumber(BigDecimal.TEN);
        documents.setCommentsstring("comment");

        warranty.setDocuments(new ArrayList<Documents>());
        warranty.getDocuments().add(documents);

        DTOIntWarranty dtoIntWarranty =  mapper.dtoIntWarrantyMap(warranty);

        Assert.assertNotNull(dtoIntWarranty);
        Assert.assertEquals(warranty.getWarrantyType().getIdstring(),dtoIntWarranty.getWarrantyType().getIdstring());
        Assert.assertEquals(warranty.getWarrantyType().getNamestring(),dtoIntWarranty.getWarrantyType().getNamestring());
        Assert.assertEquals(warranty.getValue().getAmountnumber(),dtoIntWarranty.getValue().getAmountnumber());
        Assert.assertEquals(warranty.getRegistryDatedate(),dtoIntWarranty.getRegistryDatedate());
        Assert.assertEquals(warranty.getExpeditionDatedate(),dtoIntWarranty.getExpeditionDatedate());
        Assert.assertEquals(warranty.getExpirationDatedate(),dtoIntWarranty.getExpirationDatedate());
        Assert.assertEquals(warranty.getWarrantyPlace().getCountrynumber(),dtoIntWarranty.getWarrantyPlace().getCountrynumber());
        Assert.assertEquals(warranty.getInsuranceType().getIdstring(),dtoIntWarranty.getInsuranceType().getIdstring());
        Assert.assertEquals(warranty.getInsuranceType().getNamestring(),dtoIntWarranty.getInsuranceType().getNamestring());
        Assert.assertEquals(warranty.getFeatures().getIdstring(),dtoIntWarranty.getFeatures().getIdstring());
        Assert.assertEquals(warranty.getFeatures().getDescriptionstring(),dtoIntWarranty.getFeatures().getDescriptionstring());
        Assert.assertEquals(warranty.getAppraisal().getDatestring(),dtoIntWarranty.getAppraisal().getDatestring());
        Assert.assertEquals(warranty.getAppraisal().getAppraiserstring(),dtoIntWarranty.getAppraisal().getAppraiserstring());
        Assert.assertEquals(warranty.getDocuments().get(0).getDocumentIdstring(),dtoIntWarranty.getDocuments().get(0).getDocumentIdstring());
        Assert.assertEquals(warranty.getDocuments().get(0).getTypestring(),dtoIntWarranty.getDocuments().get(0).getTypestring());
        Assert.assertEquals(warranty.getDocuments().get(0).getNumberstring(),dtoIntWarranty.getDocuments().get(0).getNumberstring());
        Assert.assertEquals(warranty.getDocuments().get(0).getOwnerstring(),dtoIntWarranty.getDocuments().get(0).getOwnerstring());
        Assert.assertEquals(warranty.getDocuments().get(0).getCreationDate(),dtoIntWarranty.getDocuments().get(0).getCreationDate());
        Assert.assertEquals(warranty.getDocuments().get(0).getExpirationDatedate(),dtoIntWarranty.getDocuments().get(0).getExpirationDatedate());
        Assert.assertEquals(warranty.getDocuments().get(0).getAssociatedValue().getTypestring(),dtoIntWarranty.getDocuments().get(0).getAssociatedValue().getTypestring());
        Assert.assertEquals(warranty.getDocuments().get(0).getAssociatedValue().getAmountnumber(),dtoIntWarranty.getDocuments().get(0).getAssociatedValue().getAmountnumber());
        Assert.assertEquals(warranty.getDocuments().get(0).getAssociatedValue().getCurrencynumber(),dtoIntWarranty.getDocuments().get(0).getAssociatedValue().getCurrencynumber());
        Assert.assertEquals(warranty.getDocuments().get(0).getCommentsstring(),dtoIntWarranty.getDocuments().get(0).getCommentsstring());
    }

}









