package com.bbva.ccol.warranty.business.impl;

import com.bbva.ccol.commons.rm.utils.converters.DateConverter;
import com.bbva.ccol.warranty.business.ISrvIntWarranty;
import com.bbva.ccol.warranty.business.dto.*;
import com.bbva.ccol.warranty.dao.IWarrantyDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.bbva.jee.arq.spring.core.servicing.test.BusinessServiceTestContextLoader;
import com.bbva.jee.arq.spring.core.servicing.test.MockInvocationContextTestExecutionListener;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;


public class SrvIntWarrantyTest {

	@InjectMocks
	ISrvIntWarranty srv;

	@Mock
	private IWarrantyDAO dao;

	@Before
	public void init() {

		srv = new SrvIntWarranty();
		MockitoAnnotations.initMocks(this);
	}

		
	@Test
	public void testCreateWarranty() {

		DTOIntWarranty dtoIntWarranty =  new DTOIntWarranty();
		dtoIntWarranty.setWarrantyType(new DTOIntWarrantyType());
		dtoIntWarranty.getWarrantyType().setIdstring("123");
		dtoIntWarranty.getWarrantyType().setNamestring("name");
		dtoIntWarranty.setValue(new DTOIntValue());
		dtoIntWarranty.getValue().setAmountnumber(BigDecimal.ONE);
		dtoIntWarranty.setRegistryDatedate(new Date());
		dtoIntWarranty.setExpeditionDatedate(new Date());
		dtoIntWarranty.setExpirationDatedate(new Date());
		dtoIntWarranty.setWarrantyPlace(new DTOIntWarrantyPlace());
		dtoIntWarranty.getWarrantyPlace().setCountrynumber(BigInteger.ONE);
		dtoIntWarranty.setInsuranceType(new DTOIntInsuranceType());
		dtoIntWarranty.getInsuranceType().setIdstring("123");
		dtoIntWarranty.getInsuranceType().setNamestring("name");
		dtoIntWarranty.setFeatures(new DTOIntFeatures());
	    dtoIntWarranty.getFeatures().setIdstring("123");
		dtoIntWarranty.getFeatures().setDescriptionstring("Description");
		dtoIntWarranty.setAppraisal(new DTOIntAppraisal());
		dtoIntWarranty.getAppraisal().setDatestring(new Date().toString());
		dtoIntWarranty.getAppraisal().setAppraiserstring("123");

		DTOIntDocuments dtoIntDocuments =  new DTOIntDocuments();
		dtoIntDocuments.setDocumentIdstring("123");
		dtoIntDocuments.setTypestring("0");
		dtoIntDocuments.setNumberstring("123");
		dtoIntDocuments.setOwnerstring("owner");
		dtoIntDocuments.setCreationDate(new Date());
		dtoIntDocuments.setExpirationDatedate(new Date());
		dtoIntDocuments.setAssociatedValue(new DTOIntAssociatedValue());
		dtoIntDocuments.getAssociatedValue().setTypestring("0");
		dtoIntDocuments.getAssociatedValue().setAmountnumber(BigDecimal.ONE);
		dtoIntDocuments.getAssociatedValue().setCurrencynumber(BigDecimal.TEN);
		dtoIntDocuments.setCommentsstring("comment");

        dtoIntWarranty.setDocuments(new ArrayList<DTOIntDocuments>());
		dtoIntWarranty.getDocuments().add(dtoIntDocuments);

		srv.createWarranty(dtoIntWarranty);

	}











	
}

